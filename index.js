// alert("hi")

let posts = [];
let count = 1;

// ADD POST

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault(); //preventing the default where the submitted information were hidden

	posts.push({

		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	count++

	showPosts(posts);
	alert("Post successfully added");
});

const showPosts = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {

		console.log(post)

		postEntries += `
		<div id="post-${post.id}">

			<h3 id="post-title-${post.id}">${post.title}</h3>

			<p id="post-body-${post.id}">${post.body}</p>

			<button onclick="editPost('${post.id}')">Edit</button>

			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>

		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};



// EDIT POST

const editPost = (id) => {

	console.log(id)

	let title = document.querySelector(`#post-title-${id}`).innerHTML;

	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;

	document.querySelector("#txt-edit-title").value = title;

	document.querySelector("#txt-edit-body").value = body;
};


// UPDATE POST

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){

			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts)
			alert("Post successfully updated")

			break;

		}
	}
});


// DELETE POST

const deletePost = (id) => {

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === `${id}`){

			posts.splice(i, 1)

			showPosts(posts)
			alert("Post successfully deleted")

			break;

		}
	}



}
